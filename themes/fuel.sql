--METADATA={ "name:fr": "Station-service", "name:en": "Fuel station", "theme:fr": "Transports", "keywords:fr": [ "station-service", "station essence", "carburant" ], "description:fr": "Stations-service issues d'OpenStreetMap (amenity=fuel)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.tags->'name' AS name, t.tags->'brand' AS brand,
	t.tags->'operator' AS operator, t.tags->'opening_hours' AS opening_hours,
	t.tags->'fuel:diesel' AS fuel_diesel, t.tags->'fuel:octane_95' AS fuel_95, t.tags->'fuel:octane_98' AS fuel_98,
	t.tags->'fuel:e10' AS fuel_e10, t.tags->'fuel:lpg' AS fuel_gpl,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.amenity = 'fuel' AND <GEOMCOND>
