--METADATA={ "name:fr": "Commerce, artisanat, bureaux", "name:en": "Shop, craft, office", "theme:fr": "Entreprise et concurrence", "keywords:fr": [ "commerce", "artisan", "bureaux" ], "description:fr": "Commerces, artisans et bureaux issus d'OpenStreetMap (shop=* ou craft=* ou office=*)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, COALESCE(t.office, t.craft, t.shop) AS type, t.tags->'name' AS name,
	t.tags->'brand' AS brand, t.tags->'operator' AS operator, t.wheelchair,
	t.tags->'opening_hours' AS opening_hours, t.tags->'level' AS level, t.tags->'ref:FR:SIRET' AS siret,
	t.tags->'wikidata' AS wikidata, t.tags->'brand:wikidata' AS brand_wikidata,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE (t.shop IS NOT NULL OR t.craft IS NOT NULL OR t.office IS NOT NULL) AND <GEOMCOND>
