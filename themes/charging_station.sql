--METADATA={ "name:fr": "Recharge de véhicule électrique (schéma Étalab IRVE 1.0.2)", "name:en": "Vehicle charging station", "theme:fr": "Transports", "keywords:fr": [ "irve", "borne de recharge électrique" ], "description:fr": "Bornes de recharge de véhicule électrique issues d'OpenStreetMap (amenity=charging_station) au format Schéma IRVE", "skipGeomCSV": true }
--GEOMETRY=point,polygon

SELECT
	t.tags->'owner' AS n_amenageur,
	t.tags->'operator' AS n_operateur,
	COALESCE(t.tags->'network', t.tags->'brand') AS n_enseigne,
	t.tags->'ref:EU:EVSE' AS id_station,
	array_to_string(array_remove(ARRAY[
		CASE WHEN t.tags->'ref' IS NOT NULL THEN  t.tags->'ref' ELSE '' END,
		CASE WHEN t.tags->'name' IS NOT NULL THEN t.tags->'name' ELSE '' END
	], ''), ' - ') AS n_station,
	CASE
		WHEN t.tags->'addr:housenumber' IS NOT NULL AND t.tags->'addr:street' IS NOT NULL THEN concat(t.tags->'addr:housenumber', ' ', t.tags->'addr:street', ', ', <ADM8NAME>)
	END AS ad_station,
	<ADM8REF> AS code_insee,
	ST_X(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)) AS "Xlongitude",
	ST_Y(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)) AS "Ylatitude",
	t.tags->'capacity' AS nbre_pdc,
	'' AS id_pdc,
	CASE WHEN t.tags->'charging_station:output' IS NOT NULL THEN replace(t.tags->'charging_station:output', ' kW', '') END AS puiss_max,
	array_to_string(array_remove(ARRAY[
		CASE WHEN t.tags->'socket:type2_combo' IS NOT NULL AND t.tags->'socket:type2_combo' != 'no' THEN 'type2_combo' ELSE '' END,
		CASE WHEN t.tags->'socket:type2' IS NOT NULL AND t.tags->'socket:type2' != 'no' THEN 'type2' ELSE '' END,
		CASE WHEN t.tags->'socket:tesla_supercharger' IS NOT NULL AND t.tags->'socket:tesla_supercharger' != 'no' THEN 'tesla_supercharger' ELSE '' END,
		CASE WHEN t.tags->'socket:type3c' IS NOT NULL AND t.tags->'socket:type3c' != 'no' THEN 'type3c' ELSE '' END,
		CASE WHEN t.tags->'socket:chademo' IS NOT NULL AND t.tags->'socket:chademo' != 'no' THEN 'chademo' ELSE '' END,
		CASE WHEN t.tags->'socket:typee' IS NOT NULL AND t.tags->'socket:typee' != 'no' THEN 'typee' ELSE '' END
	], ''), ',') AS type_prise,
	CASE
		WHEN t.tags->'fee' = 'yes' THEN 'Payant'
		WHEN t.tags->'fee' = 'no' THEN 'Gratuit'
	END AS acces_recharge,
	t.tags->'opening_hours' AS "accessibilité",
	array_to_string(array_remove(ARRAY[
		<OSMID>,
		CASE WHEN t.tags->'description' IS NOT NULL THEN t.tags->'description' ELSE '' END
	], ''), ', ') AS observations,
	replace(split_part(t.tags->'osm_timestamp', 'T', 1), '-', '/') AS date_maj,
	<GEOM>
FROM <TABLE>
WHERE t.amenity = 'charging_station' AND <GEOMCOND>
