--METADATA={ "name:fr": "Base adresses (schéma Étalab Base adresse locale 1.1.4)", "name:en": "Addresses", "theme:fr": "Infrastructures", "keywords:fr": [ "adresses", "bal", "adresse postale", "numéro de rue" ], "description:fr": "Adresses issues d'OpenStreetMap (addr:housenumber=*)", "skipGeomCSV": true }
--GEOMETRY=point,polygon

SELECT
	concat(
		<ADM8REF>,
		'_',
		CASE
			WHEN t.rel_tags->'associatedStreet_ref:FR:FANTOIR' IS NOT NULL THEN substring(t.rel_tags->'associatedStreet_ref:FR:FANTOIR', 6, 4)
			WHEN t.tags->'ref:FR:FANTOIR' IS NOT NULL THEN substring(t.tags->'ref:FR:FANTOIR', 6, 4)
			ELSE COALESCE('xxxx')
		END,
		'_',
		lpad(substring("addr:housenumber" from '^\d+'), 5, '0'),
		CASE WHEN lower(trim(both ' ' from replace(regexp_replace("addr:housenumber", '^\d+', ''), ' ', ''))) != '' THEN concat('_', lower(trim(both ' ' from replace(regexp_replace("addr:housenumber", '^\d+', ''), ' ', '')))) ELSE null END
	) AS cle_interop,
	'' AS uid_adresse,
	COALESCE(t.rel_tags->'associatedStreet_name', t.tags->'addr:street', t.tags->'addr:place', t.tags->'addr:hamlet') AS voie_nom,
	substring("addr:housenumber" from '^\d+') AS numero,
	lower(trim(both ' ' from replace(regexp_replace("addr:housenumber", '^\d+', ''), ' ', ''))) AS suffixe,
	<ADM8NAME> AS commune_nom,
	CASE
		WHEN t.tags->'entrance' IS NOT NULL THEN 'entrée'
		WHEN t.tags->'building' IS NOT NULL THEN 'bâtiment'
		ELSE 'délivrance postale'
	END AS position,
	ST_X(ST_Transform(ST_Centroid(<GEOMEMBED>), 2154)) AS x,
	ST_Y(ST_Transform(ST_Centroid(<GEOMEMBED>), 2154)) AS y,
	ST_X(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)) AS "long",
	ST_Y(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)) AS lat,
	COALESCE(t.tags->'source:addr', 'OpenStreetMap') AS source,
	substring(tags->'osm_timestamp', 1, 10) AS date_der_maj,
	<GEOM>
FROM <TABLE>
WHERE t."addr:housenumber" ~ '^\d+([a-zA-Z ]+)?$' AND <GEOMCOND>
