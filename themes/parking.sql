--METADATA={ "name:fr": "Parking (schéma Étalab Stationnement 0.1.0)", "name:en": "Parking", "theme:fr": "Transports", "keywords:fr": [ "parking" ], "description:fr": "Lieux de stationnement (vélo, voiture, deux roues) issus d'OpenStreetMap (amenity=parking|parking_space|bicycle_parking|motorcycle_parking|car_sharing) selon le schéma Stationnement de transport.data.gouv.fr", "skipGeomCSV": true }
--GEOMETRY=point,polygon

SELECT
	<OSMID> AS id,
	t.tags->'name' AS nom,
	<ADM8REF> AS insee,
	CASE WHEN t.tags->'addr:housenumber' IS NOT NULL AND t.tags->'addr:street' IS NOT NULL THEN concat(t.tags->'addr:housenumber', ' ', t.tags->'addr:street', ',', <ADM8NAME>) END AS adresse,
	COALESCE(t.tags->'website', t.tags->'contact:website') AS url,
	'tous' AS type_usagers,
	CASE
		WHEN t.tags->'fee' = 'no' OR t.tags->'fee' IS NULL THEN true
		ELSE false
	END AS gratuit,
	CASE WHEN t.amenity = 'parking' THEN t.tags->'capacity' END AS nb_places,
	'' AS nb_pr,
	CASE WHEN t.amenity = 'parking_space' AND t.wheelchair = 'yes' THEN t.tags->'capacity' ELSE t.tags->'capacity:disabled' END AS nb_pmr,
	t.tags->'capacity:charging' AS nb_voitures_electriques,
	CASE WHEN t.amenity = 'bicycle_parking' THEN t.tags->'capacity' END AS nb_velo,
	'' AS nb_2r_el,
	CASE WHEN t.amenity = 'car_sharing' THEN t.tags->'capacity' END AS nb_autopartage,
	CASE WHEN t.amenity = 'motorcycle_parking' THEN t.tags->'capacity' END AS nb_2_rm,
	CASE WHEN t.carpool = 'designated' THEN t.tags->'capacity' ELSE t.tags->'capacity:carpool' END AS nb_covoit,
	t.tags->'maxheight' AS hauteur_max,
	t.tags->'operator:ref:FR:SIRET' AS num_siret,
	ST_X(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)) AS "Xlong",
	ST_Y(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)) AS "Ylat",
	'' AS tarif_pmr,
	'' AS tarif_1h,
	'' AS tarif_2h,
	'' AS tarif_3h,
	'' AS tarif_4h,
	'' AS tarif_24h,
	'' AS abo_resident,
	'' AS abo_non_resident,
	t.tags->'parking' AS type_ouvrage,
	COALESCE(t.tags->'description', t.tags->'note') AS info,
	<GEOM>
FROM <TABLE>
WHERE
	t.amenity IN ('parking', 'parking_space', 'bicycle_parking', 'car_sharing', 'motorcycle_parking')
	AND (t.access IS NULL OR t.access NOT IN ('no', 'private'))
	AND <GEOMCOND>
