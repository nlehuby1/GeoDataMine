--METADATA={ "name:fr": "Jardins familliaux", "name:en": "Allotments", "theme:fr": "Agriculture", "keywords:fr": [ "jardins familliaux" ], "description:fr": "Jardins familliaux issus d'OpenStreetMap (landuse=allotments)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.tags->'name' AS name, t.tags->'operator' AS operator, t.tags->'plots' AS nb_parcelles,
	t.tags->'user' AS utilisateur,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.landuse = 'allotments' AND <GEOMCOND>
