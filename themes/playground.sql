--METADATA={ "name:fr": "Aire de jeux", "name:en": "Playground", "theme:fr": "Loisir", "keywords:fr": [ "aire de jeux" ], "description:fr": "Aires de jeux issues d'OpenStreetMap (leisure=playground)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.tags->'name' AS name, t.tags->'surface' AS surface, t.wheelchair,
	t.tags->'min_age' AS min_age, t.tags->'max_age' AS max_age, t.tags->'operator' AS operator,
	t.tags->'opening_hours' AS opening_hours, t.access, t.tags->'indoor' AS indoor, t.tags->'fee' AS fee,
	t.tags->'supervised' AS supervised,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.leisure = 'playground' AND <GEOMCOND>
