--METADATA={ "name:fr": "Équipement de lutte contre les incendies", "name:en": "Anti-fire devices", "theme:fr": "Sécurité", "keywords:fr": [ "incendie", "borne", "poteau", "colonne sèche", "hydrant" ], "description:fr": "Équipements permettant de lutter contre les incendies issus d'OpenStreetMap (emergency=dry_riser_inlet|fire_extinguisher|fire_flapper|fire_hose|fire_hydrant|water_tank|fire_water_pond|suction_point)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.emergency AS type, t.tags->'ref' AS "ref",
	t.tags->'fire_hydrant:type' AS fire_hydrant_type, t.tags->'fire_hydrant:pressure' AS pressure,
	t.tags->'fire_hydrant:diameter' AS diameter, t.tags->'colour' AS colour, t.tags->'flow_rate' AS flow_rate,
	t.tags->'couplings' AS couplings, t.tags->'water_source' AS water_source, t.tags->'survey:date' AS survey_date,
	t.tags->'fire_hydrant:position' AS position, t.tags->'water_tank:volume' AS volume,
	t.tags->'indoor' AS indoor, t.tags->'level' AS level,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.emergency IN ('dry_riser_inlet', 'fire_extinguisher', 'fire_flapper', 'fire_hose', 'fire_hydrant', 'water_tank', 'fire_water_pond', 'suction_point') AND <GEOMCOND>
