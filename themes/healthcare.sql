--METADATA={ "name:fr": "Santé (hôpital, clinique, pharmacie)", "name:en": "Healthcare equipment", "theme:fr": "Santé", "keywords:fr": [ "hôpital", "clinique", "médecin", "pharmacie" ], "description:fr": "Établissements liés à la santé issus d'OpenStreetMap (amenity=hospital|clinic|dentist|doctors|nursing_home|pharmacy)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.amenity AS type, t.tags->'name' AS name,
	t.tags->'operator' AS operator, t.tags->'emergency' AS emergency, t.wheelchair,
	t.tags->'opening_hours' AS opening_hours, t.tags->'ref:FR:FINESS' AS ref_finess,
	t.tags->'type:FR:FINESS' AS type_finess, t.tags->'ref:FR:NAF' AS ref_naf, t.tags->'capacity' AS capacity,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.amenity IN ('hospital', 'clinic', 'dentist', 'doctors', 'nursing_home', 'pharmacy') AND <GEOMCOND>
