--METADATA={ "name:fr": "Parking à vélos", "name:en": "Bicycle parking", "theme:fr": "Transports", "keywords:fr": [ "vélo", "parking à vélos" ], "description:fr": "Parkings à vélos issus d'OpenStreetMap (amenity=bicycle_parking)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.tags->'name' AS name, t.access, t.tags->'operator' AS operator,
	t.tags->'covered' AS covered, t.tags->'capacity' AS capacity,
	t.tags->'bicycle_parking' AS type,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.amenity = 'bicycle_parking' AND <GEOMCOND>
