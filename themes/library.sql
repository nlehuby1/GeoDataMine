--METADATA={ "name:fr": "Bibliothèque", "name:en": "Library", "theme:fr": "Culture", "keywords:fr": [ "bibliothèque" ], "description:fr": "Bibliothèques et micro-bibliothèques issues d'OpenStreetMap (amenity=library|public_bookcase)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.amenity AS type, t.tags->'name' AS name, t.tags->'ref:isil' AS ref_isil,
	t.tags->'operator' AS operator, t.wheelchair, t.tags->'opening_hours' AS opening_hours,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.amenity IN ('library', 'public_bookcase') AND <GEOMCOND>
