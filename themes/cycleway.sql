--METADATA={ "name:fr": "Piste/bande cyclable", "name:en": "Cycleway", "theme:fr": "Transports", "keywords:fr": [ "piste cyclable", "bande cyclable" ], "description:fr": "Pistes et bandes cyclables issues d'OpenStreetMap (cycleway=* ou cycleway:right=* ou cycleway:left=* ou cycleway:both=* ou highway=cycleway)" }
--GEOMETRY=line

SELECT
	<OSMID> as osm_id, t.cycleway, t.highway, t."cycleway:right", t."cycleway:left", t."cycleway:both", t.tags->'name' AS name,
	t.tags->'surface' AS surface, t.tags->'oneway' AS oneway, t.tags->'bridge' AS bridge, t.tags->'tunnel' AS tunnel,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE (t.cycleway IS NOT NULL OR t.highway = 'cycleway' OR t."cycleway:right" IS NOT NULL OR t."cycleway:left" IS NOT NULL OR t."cycleway:both" IS NOT NULL) AND <GEOMCOND>
