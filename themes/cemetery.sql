--METADATA={ "name:fr": "Cimetière", "name:en": "Cemetery", "theme:fr": "Infrastructures", "keywords:fr": [ "cimetière" ], "description:fr": "Cimetières issus d'OpenStreetMap (landuse=cemetery)" }
--GEOMETRY=polygon

SELECT
	<OSMID> as osm_id, t.tags->'name' AS name, t.tags->'religion' AS religion, t.tags->'denomination' AS religion_denomination,
	t.tags->'operator' AS operator, t.wheelchair, t.tags->'opening_hours' AS opening_hours,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.landuse = 'cemetery' AND <GEOMCOND>
