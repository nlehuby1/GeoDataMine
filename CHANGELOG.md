# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Next version (to be released)


## 0.2.0 - 2020-03-09

### Changed
- Database import uses `osm2pgsql` Flex back-end
- Addresses theme is now able to retrieve street names defined in `associatedStreet` relations


## 0.1.2 - 2020-02-11

### Changed
- Added "ref:EU:EVSE" tag in charging station theme
- Updated defibrillator theme according to changes in ARS DAE schema


## 0.1.1 - 2020-01-07

### Added
- Support of XLSX format
- Changelog file

### Fixed
- Links and some install commands of Readme


## 0.1.0 - 2019-12-30

### Added
- Initial release (UI, backend, various themes)
