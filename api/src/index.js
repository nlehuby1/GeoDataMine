/**
 * API main code
 */

const CONFIG = require('../../config.json');
const express = require('express');
const cors = require('cors');
const compression = require("compression");
const fs = require('fs');
const db = require('./db');
const themes = require('./themes');
const FORMAT_TO_MIME = { "csv": "text/csv", "shapefile": "application/zip", "geojson": "application/vnd.geo+json", "zip": "application/zip", "xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" };
const FORMAT_TO_EXT = { "csv": "csv", "shapefile": "shp.zip", "geojson": "geojson", "zip": "zip", "xlsx": "xlsx" };

// Init API
const app = express();
const port = process.env.PORT || 3000;
app.use(cors());
app.options('*', cors());
app.use(compression());

// Log queries
app.use((req, res, next) => {
	const toLog = {
		ip: req.ip,
		path: req.path,
		query: req.query,
		ts: Date.now()
	};

	fs.appendFile('api.log', JSON.stringify(toLog)+'\n', (err) => {
		if(err) {
			console.log("Can't write into log file", err);
		}
	});

	next();
});


/*
 * List of routes
 */

/**
 * @api {get} / Simple website
 * @apiDescription Get the website for extracting data from API
 * @apiName GetSite
 * @apiGroup Default
 */
app.use('/', express.static(__dirname+'/../build'));

/**
 * @api {get} /doc API documentation
 * @apiDescription Get the user documentation of the API (this page)
 * @apiName GetDoc
 * @apiGroup Default
 */
app.use('/doc', express.static(__dirname+'/../doc'));

/**
 * @api {get} /data/:themeId/:boundaryId Thematic data extract
 * @apiDescription Get OpenStreetMap data for given theme, in given administrative boundary
 * @apiName GetData
 * @apiGroup Default
 *
 * @apiParam {String} themeId Theme of data to extract (one in /themes list)
 * @apiParam {String} boundaryId Boundary of area where data must be extracted (one in /boundaries list)
 * @apiParam {String} [format=csv] Output file format (csv, geojson, shapefile, xlsx)
 * @apiParam {Number} [radius] Filter by radius around boundary centroid (in kilometers) instead of boundary polygon (max 50)
 * @apiParam {Boolean} [aspoint=false] Geometry will always be point if set to true (center of complete geometry)
 * @apiParam {Boolean} [metadata=false] Add a metadata file for the generated dataset, in <a href="https://scdl.opendatafrance.net/docs/schemas/catalogue.html">SCDL format</a>
 *
 * @apiExample As GeoJSON:
 *     http://localhost:3000/data/bicycle_parking/1234?format=geojson
 * @apiExample As CSV, using radius:
 *     http://localhost:3000/data/bicycle_parking/1234?format=csv&radius=5
 */
app.get("/data/:themeId/:boundaryId", (req, res) => {
	// Check format
	const formats = Object.keys(FORMAT_TO_EXT);
	const format = req.query.format || "csv";
	if(!formats.includes(format)) {
		return res.status(400).send("format parameter is invalid (should be "+formats.join(", ")+")");
	}

	// Check radius
	const radius = req.query.radius || -1;
	if(isNaN(parseFloat(radius))) {
		return res.status(400).send("radius should either not be defined, or be a positive number");
	}
	else if(parseFloat(radius) > 50) {
		return res.status(400).send("radius should be <= 50 km");
	}

	// Check theme ID
	const theme = themes.full.find(t => t.id === req.params.themeId);
	if(!theme) {
		return res.status(400).send("Theme ID is invalid, please use one ID given on /themes URL");
	}

	// Check aspoint
	const aspoint = req.query.aspoint === "true";

	// Check metadata
	const metadata = req.query.metadata === "true";

	// Save used URL (for metadata)
	const myPort = req.app.settings.port || port;
	const baseUrl = CONFIG.API_BASE_URL ? CONFIG.API_BASE_URL : req.protocol + '://' + req.hostname + (myPort == 80 || myPort == 443 ? '' : ':' + myPort);
	const url = baseUrl  + req.originalUrl;

	// Retrieve data
	const boundaryId = req.params.boundaryId;
	db.getBoundary(boundaryId)
	.then(boundary => {
		return db.getThemeData(theme, boundary, { format: format, radius: radius, aspoint: aspoint, metadata: metadata, url: url })
		.then(filename => {
			// Read file and send it
			const stat = fs.statSync(filename);
			const day = (new Date()).toISOString().split("T")[0];
			const finalFormat = metadata ? "zip" : format;

			res.writeHead(200, {
				'Content-Type': FORMAT_TO_MIME[finalFormat],
				'Content-Length': stat.size,
				'Content-Disposition': `attachment; filename="${boundary.simple_name}_${theme.id}_${day}.${FORMAT_TO_EXT[finalFormat]}"`
			});

			var readStream = fs.createReadStream(filename);
			readStream.pipe(res);

			// Handle errors on sending
			readStream.on('error', (err) => {
				readStream.end();
			});

			// Delete output file
			readStream.on('end', () => {
				fs.unlink(filename, (err) => {
					if(err) {
						console.log("Can't delete tmp file: ", err);
					}
				});
			});
		});
	})
	.catch(e => {
		console.log(e);
		res.status(500).send("Can't extract data: "+e.message);
	});
});

/**
 * @api {get} /themes List of themes
 * @apiDescription Get all data themes available
 * @apiName GetThemes
 * @apiGroup Default
 *
 * @apiSuccessExample {json} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "themes": [
 *        {
 *          "id": "bicycle_parking",
 *          "name:en": "Bicycle parkings",
 *          "name:fr": "Parking à vélo"
 *        },
 *        ...
 *     ]
 *   }
 */
app.get("/themes", (req, res) => {
	res.send({ themes: themes.display });
});

/**
 * @api {get} /boundaries List of administrative boundaries
 * @apiDescription Get all administrative boundaries that can be used to filter data queries
 * @apiName GetBoundaries
 * @apiGroup Default
 *
 * @apiSuccessExample {json} Success-Response:
 *   HTTP/1.1 200 OK
 *   [
 *     {
 *       "id": "1234",
 *       "name": "Rennes",
 *       "type": "admin_8"
 *     },
 *     ...
 *   ]
 */
app.get("/boundaries", (req, res) => {
	db.getBoundaries()
	.then(b => res.send(b))
	.catch(e => {
		console.log(e);
		res.status(500).send("Can't read boundaries data");
	});
});

/**
 * @api {get} /boundaries/search Search administrative boundaries
 * @apiDescription Get administrative boundaries matching search pattern
 * @apiName GetBoundariesSearch
 * @apiGroup Default
 * @apiParam {String} text Search pattern (can be name, ref)
 *
 * @apiSuccessExample {json} Success-Response:
 *   HTTP/1.1 200 OK
 *   [
 *     {
 *       "id": "1234",
 *       "name": "Rennes",
 *       "type": "admin_8"
 *     },
 *     ...
 *   ]
 */
app.get("/boundaries/search", (req, res) => {
	const text = req.query.text.trim() || "";
	if(text.length === 0) {
		return res.send([]);
	}

	db.findBoundariesByName(text)
	.then(b => res.send(b))
	.catch(e => {
		console.log(e);
		res.status(500).send("Can't search in boundaries: " + e.message);
	});
});

// 404
app.use((req, res) => {
	res.status(404).send(req.originalUrl + ' not found')
});


// Start
app.listen(port, () => {
	console.log('API started on port: ' + port);
});
