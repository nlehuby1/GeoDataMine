/**
 * Allows reading SQL files containing themes metadata
 */

const fs = require('fs');
const folder = '../themes';
const sqlrgx = /^[A-Za-z0-9_\-]+\.sql$/;
const themes = [];
const themesLight = [];

fs.readdirSync(folder).forEach((file) => {
	if(sqlrgx.test(file)) {
		try {
			const themeId = file.substring(0, file.length - 4);
			const themeTxt = fs.readFileSync(folder+"/"+file, 'utf8');

			// Parse file
			let metadata, geom;
			let sql = "";
			themeTxt.split("\n").forEach(line => {
				if(line.startsWith("--METADATA=")) {
					metadata = JSON.parse(line.substring(11).trim());
				}
				else if(line.startsWith("--GEOMETRY=")) {
					geom = line.substring(11).trim().split(",");
					if(geom.findIndex(g => !["point", "line", "polygon"].includes(g)) >= 0) {
						throw new Error("invalid geometry type");
					}
				}
				else if(line.trim().length > 0) {
					sql += line.trim() + " ";
				}
			});

			// Append data
			themesLight.push(Object.assign({ id: themeId }, metadata));
			themes.push(Object.assign({ id: themeId, sql: sql, geomtypes: geom }, metadata));
		}
		catch(e) {
			throw new Error("Invalid theme file: "+file+" "+e.message);
		}
	}
	else {
		console.log("Ignored file: "+file);
	}
});

module.exports = {
	display: themesLight,
	full: themes
};
