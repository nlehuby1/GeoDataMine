const data = {};
const url = window.location.origin + window.location.pathname.substring(0, window.location.pathname.lastIndexOf('/')) + "/";
const BOUNDARY_TYPES = {"admin_6": "Département", "admin_7": "Pays (loi Voynet)", "admin_8": "Commune", "political": "Canton", "local_authority": "EPCI"};
let selectedBoundary = null;
let timerSearchInput = null;
let hasPrevTextSearch = false;

setDownloadable(false);


/*
 * Data download from API
 */
function request(url) {
	return new Promise((resolve, reject) => {
		var xhr = new XMLHttpRequest();
		xhr.addEventListener("load", () => {
			resolve(xhr.responseText);
		});
		xhr.addEventListener("error", reject);
		xhr.addEventListener("abort", reject);
		xhr.open("GET", url);
		xhr.send();
	});
}

request(url+"themes")
	.then(themes => {
		data.themes = JSON.parse(themes).themes;
		data.themes.sort((a,b) => a["name:fr"].localeCompare(b["name:fr"]));
	})
	.catch(e => {
		data.themes = "error";
		console.error(e);
	});


/*
 * Populate form using received data
 */
function createForm() {
	// Prepare form
	const formExtract = document.getElementById("form-extract");

	// Show theme list
	const selectTheme = document.getElementById("theme");
	selectTheme.innerHTML = "";
	selectTheme.addEventListener("change", () => setDownloadable(true));

	data.themes.forEach(theme => {
		const tdom = document.createElement("option");
		tdom.value = theme.id;
		tdom.innerHTML = theme["name:fr"];
		selectTheme.appendChild(tdom);
	});

	// Set-up boundary search
	const searchTextField = document.getElementById("boundary-search");
	const searchSuggestions = document.getElementById("boundary-suggestions");
	searchTextField.value = "";

	searchTextField.addEventListener("input", e => {
		if(timerSearchInput) {
			clearTimeout(timerSearchInput);
		}

		$('#boundary-search').dropdown('show');
		selectedBoundary = null;
		setDownloadable(false);

		const text = e.target.value;
		if(!text || text.trim().length === 0) {
			searchSuggestions.innerHTML = '<a class="dropdown-item disabled">Tapez un nom pour lancer la recherche</a>';
			hasPrevTextSearch = false;
		}
		else {
			const waitText = '<a class="dropdown-item disabled"><span class="spinner-border text-primary" role="status"></span></a>';
			if(!hasPrevTextSearch && searchSuggestions.innerHTML !== waitText) {
				searchSuggestions.innerHTML = waitText;
			}

			timerSearchInput = setTimeout(() => {
				hasPrevTextSearch = true;
				request(url + "boundaries/search?text="+encodeURIComponent(text))
				.then(txt => JSON.parse(txt))
				.then(results => {
					searchSuggestions.innerHTML = '';

					if(results.length > 0) {
						results.forEach(r => {
							const type = BOUNDARY_TYPES[r.type];
							const ref = r.ref || "ID inconnu";
							const entry = document.createElement("a");
							entry.classList.add("dropdown-item");
							entry.innerHTML = r.name + ' <small>(' + type + ' - ' + ref + ')</small>';
							searchSuggestions.appendChild(entry);
							entry.addEventListener("click", () => {
								searchTextField.value = r.name + ' (' + type + ' - ' + ref + ')';
								selectedBoundary = r.id;
								setDownloadable(true);
							});
						});
					}
					else {
						searchSuggestions.innerHTML = '<a class="dropdown-item disabled">Aucun résultat</a>';
					}
				})
				.catch(e => {
					console.log(e);
					searchSuggestions.innerHTML = '<a class="dropdown-item disabled">Erreur</a>';
				});
			}, 250);
		}
	});


	// Radius selector with special value 0
	const lblRadius = document.getElementById("radius-val");
	const iptRadius = document.getElementById("radius");
	iptRadius.value = "0";
	iptRadius.addEventListener("input", () => {
		const newVal = parseInt(document.getElementById("radius").value);
		lblRadius.innerHTML = newVal === 0 ? "Emprise de la zone sélectionnée" : newVal + "km autour du centre géométrique";
		setDownloadable(true);
	});

	// Hide loader
	const formLoad = document.getElementById("form-load");
	formLoad.classList.add("d-none");
	formExtract.classList.remove("d-none");

	// Follow options updates
	document.getElementById("format").addEventListener("change", () => setDownloadable(true));
	document.getElementById("aspoint").addEventListener("change", () => setDownloadable(true));
	document.getElementById("metadata").addEventListener("change", () => setDownloadable(true));
}


/*
 * Update nodes to allow download start or not
 */
function setDownloadable(shouldI) {
	const btnDl = document.getElementById("launch");
	const permalink = document.getElementById("permalink");
	const permabloc = document.getElementById("permabloc");
	const permamiss = document.getElementById("permalink-missing");
	const theme = document.getElementById("theme").value;
	const iptRadius = document.getElementById("radius");

	if(shouldI && selectedBoundary && theme && theme.length > 0) {
		const opts = {
			format: document.getElementById("format").value,
			aspoint: document.getElementById("aspoint").checked ? "true" : "",
			metadata: document.getElementById("metadata").checked ? "true" : "",
			radius: parseInt(iptRadius.value) === 0 ? null : iptRadius.value
		};

		let dlUrl = url+"data/"+theme+"/"+selectedBoundary+"?";
		dlUrl += Object.entries(opts).filter(o => o[1] && o[1] !== null && o[1].length > 0).map(o => o.join("=")).join("&");

		permabloc.classList.remove("d-none");
		launch.disabled = false;
		permalink.value = dlUrl;
		btnDl.href = dlUrl;
		btnDl.classList.remove("disabled");
	}
	else {
		permabloc.classList.add("d-none");
		launch.disabled = true;
		permalink.value = "";
		btnDl.href = "";
		btnDl.classList.add("disabled");
	}
}

/*
 * Wait for data retrieval
 */
const itv = setInterval(() => {
	if(data && data.themes) {
		clearInterval(itv);

		if(data.themes === "error") {
			document.getElementById("form-failed").classList.remove("d-none");
			document.getElementById("form-load").classList.add("d-none");
		}
		else {
			createForm();
		}
	}
}, 100);
